L'anglais (English en anglais ; prononcé : /ˈɪŋ.ɡlɪʃ/) est une langue indo-européenne germanique originaire d'Angleterre qui tire ses racines de langues du nord de l'Europe (terre d'origine des Angles, des Saxons et des Frisons) dont le vocabulaire a été enrichi et la syntaxe et la grammaire modifiées par la langue normande apportée par les Normands, puis par le français avec les Plantagenêt. La langue anglaise est ainsi composée d'environ 60 à 70 % de mots d'origine normande et française3,4. L'anglais est également très influencé par les langues romanes, en particulier par l'utilisation de l'alphabet latin ainsi que les chiffres arabes.

Langue officielle de facto du Royaume-Uni, de l'Irlande et d'autres îles de l'archipel britannique (Île de Man, îles anglo-normandes), l'anglais est la langue maternelle de tout ou partie de la population, et suivant les cas, la langue ou une des langues officielles de plusieurs pays, totalement ou partiellement issus des anciennes colonies britanniques de peuplement, dont les États-Unis, le Canada, l'Australie et la Nouvelle-Zélande, que l'on réunit sous l'appellation de monde anglo-saxon, bien qu'il n'existe pas de définition universelle de cette expression.

Il est également langue officielle ou langue d'échange dans de très nombreux pays issus de l'ancien Empire britannique, même en l'absence de population d'origine anglo-saxonne significative (Kenya, Nigeria, Hong-Kong, Inde, Pakistan, etc.).

Beaucoup de pays dont l'anglais est la langue officielle sont réunis au sein du Commonwealth (bien que pour certains, il ne soit pas l'unique langue officielle).

Il est également l'une des vingt-quatre langues officielles de l'Union européenne.

L'anglais est l'une des langues les plus parlées au monde : par son nombre de locuteurs de naissance5, il se classe troisième, après le chinois (mandarin) qui occupe le premier rang, et l'espagnol, qui occupe le second.

Considérée par beaucoup comme étant la langue internationale prédominante6, elle est la langue la plus souvent enseignée en tant que langue étrangère à travers le monde7. Il est également la langue la plus utilisée sur internet8. L'anglais est l'une des six langues officielles et des deux langues de travail — avec le français — de l'Organisation des Nations unies (ONU). 
