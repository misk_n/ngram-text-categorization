N-gram-based Text Categorization
================================

## Introduction

This project aims at implementing a N-gram text categorization, and use it to compute 
the most significant N-grams by TF-IDF.
The categorization is typically used in order to determine a document's language. 
The TF-IDF is then computed relatively to the document's language training corpus.

## Quick start

Usage:

``` 
> python3 batch.py [training corpus] [test corpus]
```

Where the corpus are folders structured as follows:

```
+ /training_corpus
+-- category1
|   +-- file1
|   +-- file2
|   +-- file3
|    ...
+-- category2
|   +-- file1
|   +-- file2
|   +-- file3
|    ...
 ...
```

Please note that a category name will be its folder name.

## Tests

The provided tests in the "tests" folder are for language categorization.
```
> python3 batch.py ./tests ./tests
```
Should label each file without any mistake. You can try this with your own testing corpus

## Notes on the method used

### Program: batch.py

This program can be divided into two steps:

#### Step 1: labelling the document

From the given training corpus, this software will compute the N-gram profile 
of each training corpus' categories, and the N-gram profile of each file of 
the test corpus. It will then perform the classification of each document in the
test corpus against those categories.

For more information on this classification method, please refer to this paper :

**N-gram-based Text Categorization** (William B. Cavnar and John M. Trenkle) - 
*Environmental Research Institute of Michigan*

Note that we use word N-grams instead of characters. The results are quite the same.

#### Step 2: computing the most significant N-grams

For each file in the test corpus, this software will print the 5 most significant N-grams and
their TF-IDF. TF-IDF is usually used on stemmed and lemmatized words of a document.
This is a task asked for this school project.
However, the TF-IDF on N-grams, even if less readable, have its meaning : considering
the application of auto-completion, a program performing this task could look for the
most significant N-gram to complete a word.
The IDF of a file is computed relatively to the documents of the category assigned to this 
file during step 1.

### Program: console.py

This program is a tool for document writing. Given a corpus and some inputs, it will provide
the most pertinent following outputs the user can give, according the TF-IDF.

The method used is basically the same as for batch.py, except that the program gets and maintains
the user's inputs, automatically detects the category of the text, and gives some pertinent words
to write next at each iteration of the main loop.
