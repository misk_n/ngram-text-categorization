import os
import document
import math


class Category:

    def read_documents(self):
        for file in os.listdir(self.corpus_path):
            file_path = os.path.join(self.corpus_path, file)
            try:
                doc = document.Document(file_path)
            except UnicodeDecodeError:
                continue
            self.documents.append(doc)
            for n_gram, occ in doc.n_grams.items():
                if n_gram not in self.n_grams:
                    self.n_grams[n_gram] = occ
                else:
                    self.n_grams[n_gram] += occ
            if not self.keep_document:
                doc.empty_text()
        self.profile = list(map(lambda p: p[0], sorted(self.n_grams.items(), key=lambda p: -p[1])))
        print('End of ' + self.name + " N-gram computation")

    def compute_profile_distance(self, doc, max=1000):
        distance = 0
        count = 0
        for n_gram in self.profile:
            if count == max:
                break
            if n_gram not in doc.n_grams:
                distance += max
            else:
                distance += abs(doc.profile.index(n_gram) - count)
            count += 1
        return distance

    def compute_idf(self, n_gram):
        corpus_size = len(self.documents)
        n_gram_occur = 0
        for doc in self.documents:
            if n_gram in doc.profile:
                n_gram_occur += 1
        try:
            res = math.log(corpus_size / (1 + n_gram_occur))
        except ValueError:
            print("Invalid values for idf computations of N-gram: " + n_gram)
            print("corpus size: " + str(corpus_size))
            print("N-gram occurence + 1: " + str(1 + n_gram_occur))
            return -1
        return res

    def __init__(self, corpus_path, name, keep_documents=False):
        self.name = name
        self.keep_document = keep_documents
        self.corpus_path = corpus_path
        self.documents = []
        self.n_grams = {}
        self.profile = []
        self.read_documents()
