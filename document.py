class Document:

    def compute_n_grams(self, s, n):
        first = "_ "
        first += ' '.join(s[0:n - 1])
        self.n_grams[first] = 1
        for i in range(len(s)):
            n_gram = ' '.join(s[i:i + n])
            u = len(n_gram)
            for j in range(n - u):
                n_gram += ' _'
            if n_gram not in self.n_grams:
                self.n_grams[n_gram] = 1
            else:
                self.n_grams[n_gram] += 1
            if self.max_occur < self.n_grams[n_gram]:
                self.max_occur = self.n_grams[n_gram]
                self.max_occur_n_gram = n_gram

    def compute_tf(self, n_gram):
        if n_gram not in self.n_grams:
            return 0
        return self.n_grams[n_gram] / self.max_occur

    def read_file(self):
        with open(self.path) as f:
            work_string = []
            for line in f.readlines():
                line = ''.join(e for e in line if e.isalpha() or e == '\'' or e.isspace())
                work_string += line.split()
            self.text = work_string

    def empty_text(self):
        del self.text
        del self.n_grams

    def __init__(self, document_path='', text=None):
        if text is None:
            self.path = document_path
            self.text = ''
            self.read_file()
        else:
            self.text = text

        self.max_occur = 0
        self.max_occur_n_gram = ''
        self.n_grams = {}

        self.compute_n_grams(self.text, 2)
        self.compute_n_grams(self.text, 3)
        self.compute_n_grams(self.text, 4)

        self.profile = list(map(lambda p: p[0], sorted(self.n_grams.items(), key=lambda p: -p[1])))
