import os
import category


def get_categories(path, keep_documents=False):
    categories = []
    for category_name in os.listdir(path):
        category_path = os.path.join(path, category_name)
        categories += [category.Category(category_path, category_name, keep_documents)]
    return categories


def compute_tf_idfs(doc, cat):
    tf_idfs = {}
    for n_gram in doc.profile:
        tf = doc.compute_tf(n_gram)
        idf = cat.compute_idf(n_gram)
        tf_idfs[n_gram] = tf * idf
    return tf_idfs
