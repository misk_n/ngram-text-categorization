import document
import util
import sys


def main():
    if len(sys.argv) != 2:
        print("usage: " + sys.argv[0] + " [training corpus directory]")
        exit(1)

    categories = util.get_categories(sys.argv[1])

    print("Type your first word and press Enter to start")
    print("Enter an empty word to exit")
    inputs = ""
    while True:
        print("next word > ", end='', flush=True)
        cur = input()
        if cur == '':
            break
        inputs += ' ' + cur
        doc = document.Document(text=inputs)
        label = None
        distance = float('inf')
        for cat in categories:
            d = cat.compute_profile_distance(doc, 300)
            if d < distance:
                distance = d
                label = cat
        print("Detected category: " + label.name)
        suggestions = {}
        for n_gram in label.profile:
            if n_gram.split()[0] == cur:
                suggestions[n_gram.split()[1]] = None
            if len(suggestions):
                break
        for s in suggestions:
            print("suggestion: " + s)

    print("Entire text:")
    print(inputs)


main()
