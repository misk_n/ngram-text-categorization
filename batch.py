import util
import sys
import os
import document


def perform_labelling(k=10):
    categories = util.get_categories(sys.argv[1])

    for directory in os.listdir(sys.argv[2]):
        for filename in os.listdir(os.path.join(sys.argv[2], directory)):
            file_path = os.path.join(sys.argv[2], directory, filename)
            try:
                test_doc = document.Document(file_path)
            except UnicodeDecodeError:
                continue
            print("Processing: " + test_doc.path)
            distance = float('inf')
            label = None
            for cat in categories:
                d = cat.compute_profile_distance(test_doc, 300)
                if d < distance:
                    distance = d
                    label = cat
            print("+- This document is categorized as " + label.name)
            tf_idfs = util.compute_tf_idfs(test_doc, label)
            most_significant_n_grams = list(sorted(tf_idfs.items(), key=lambda p: -p[1])[0:k])
            print("+- " + str(k) + " most significant N-grams with their TF-IDF: " + str(most_significant_n_grams))


def main():
    if len(sys.argv) != 3:
        print("usage: " + sys.argv[0] + " [training corpus directory] [testing corpus directory]")
        exit(1)

    perform_labelling(5)


main()
